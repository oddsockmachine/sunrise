# from types import SimpleNamespace

# ctx1 = SimpleNamespace(**{
#     'brightness': 0.5,
#     'gamma': 1,
#     'orientation': 'zigzag',
#     # 'size': (30, 10),
#     'size': (3, 5),
#     'run': True,
#     'location': (0.12345, 1.23450),
#     'mode': {
#         'type': 'sunrise',
#         'theme': 'miami',
#         'start_time': "HHMMSS",
#     },
#     'overlay': {
#         'type': 'clock',
#         'style': 'dim'
#     }
#     # 'mode': {
#     #     'type': 'weather',
#     #     'weather': 'partly cloudy',
#     # },
#     # 'overlay': {
#     #     'type': 'alert',
#     #     'message': 'Doorbell'
#     # }
# })

# ctx2 = SimpleNamespace(**{
#     'brightness': 0.5,
#     'gamma': 1,
#     'orientation': 'zigzag',
#     # 'size': (30, 10),
#     'size': (3, 5),
#     'run': True,
#     'location': (0.12345, 1.23450),
#     'mode': {
#         'type': 'sunrise',
#         'theme': 'CALI',
#         'start_time': "HHMMSS",
#     },
#     'overlay': {
#         'type': 'clock',
#         'style': 'dim'
#     }
#     # 'mode': {
#     #     'type': 'weather',
#     #     'weather': 'partly cloudy',
#     # },
#     # 'overlay': {
#     #     'type': 'alert',
#     #     'message': 'Doorbell'
#     # }
# })

# ctx3 = SimpleNamespace(**{
#     'brightness': 0.5,
#     'gamma': 1,
#     'orientation': 'zigzag',
#     # 'size': (30, 10),
#     'size': (3, 5),
#     'run': True,
#     'location': (0.12345, 1.23450),
#     'mode': {
#         'type': 'weather',
#         'weather': 'partly cloudy',
#     },
#     'overlay': {
#         'type': 'alert',
#         'message': 'Doorbell'
#     }
# })
# ctx4 = SimpleNamespace(**{
#     'brightness': 0.5,
#     'gamma': 1,
#     'orientation': 'zigzag',
#     # 'size': (30, 10),
#     'size': (3, 5),
#     'run': True,
#     'location': (0.12345, 1.23450),
#     'mode': {
#         'type': 'sunrise',
#         'theme': 'miami',
#         'start_time': "HHMMSS",
#     },
#     'overlay': {
#         'type': 'clock',
#         'style': 'dim'
#     }
#     # 'mode': {
#     #     'type': 'weather',
#     #     'weather': 'partly cloudy',
#     # },
#     # 'overlay': {
#     #     'type': 'alert',
#     #     'message': 'Doorbell'
#     # }
# })

# print(ctx1 == ctx2)
# print(ctx1 == ctx3)
# print(ctx2 == ctx3)
# print(ctx1 == ctx4)


# from sty import bg, fg, rs
# x = '▓▓'
# a = fg(10, 255, 10) + "I have a green foreground." + rs.fg
# b = bg(255, 150, 50) + "I have an orange background" + rs.bg
# c = fg(90, 90, 90) + bg(32, 32, 32) + "Grey fg and dark grey bg." + rs.all

# count = 0
# for i in range(255)[0:255:20]:
#     for j in range(255)[0:255:20]:
#         for k in range(255)[0:255:20]:
#             count += 1
#             print(fg(i,j,k) + x + rs.fg, end="")
#             if count % 80 == 0:
#                 print("")

# def display(grid):
#     o = '▓▓'
#     for row in grid:
#         for x in row:
#             print(x)
#             print(fg(x[0],x[1],x[2]) + o + rs.fg, end="")
#         print("")

# import colorsys
# print(colorsys.hsv_to_rgb(0.5, 0.5, 0.4))

# for each time slice
# find max and min gradient positions
# map each gradient position, gp, to a float proportion 0-1
# for each led position in the y direction, calculate their float position 0-1
# find which 2 gradient slices we're between (or just 1, if at 0 or 1)
# Blend between Hls values. H requires direction awareness, ability to go over 1, SL can be linear
# Apply blended value to all leds across x
# Store/cache these gradients for each time slice
# Do the same interpolation between time slices:
# find when we are, between two time slices
# tp = proportion between t1 and t2
# for t1 and t2, calculate their gradients as above

import yaml
from pprint import pprint
import colorsys
from sty import bg, fg, rs

w = 10
h = 30

def color_blend(x, l, u):
    a = False
    if a:
        return hls_blend(x,l,u)
    else:
        return rgb_blend(x,l,u)

def normalize(x, min, max):
    g_range = max - min
    offset = x - min
    n = offset / g_range
    return n

def linear_blend(x, l, u):
    if l == u:
        return l
    b = l + ((u - l) * x)
    # print(f"x: {x}, l: {l}, u: {u}  =  {b}")
    return b

def hue_blend(x, lower, upper):
    if upper - lower > 0.5:
        upper -= 1
        # print("!")
        return 1+linear_blend(x, lower, upper)
    return linear_blend(x, lower, upper)

def hls_blend(x, l, u):
    hls_l = colorsys.rgb_to_hls(l[0],l[1],l[2])
    hls_u = colorsys.rgb_to_hls(u[0],u[1],u[2])
    # print(hls_l)
    blend = (
        hue_blend(x, hls_l[0], hls_u[0]),
        linear_blend(x, hls_l[1], hls_u[1]),
        linear_blend(x, hls_l[2], hls_u[2]),
    )
    rgb = colorsys.hls_to_rgb(blend[0], blend[1], blend[2])
    return rgb

def rgb_blend(x, l, u):
    rgb = (
        linear_blend(x, l[0], u[0]),
        linear_blend(x, l[1], u[1]),
        linear_blend(x, l[2], u[2]),
    )
    return rgb

def select_inner_grad(norm_height, grad_keys):
    # Convert from a multi-part gradient to a single color pair w ratio position
    if norm_height in grad_keys:
        # Special case where position is exactly aligned with a key
        lower = norm_height
        upper = norm_height
        pos = 0.5
        return pos, lower, upper
    for i, v in enumerate(grad_keys):
        if norm_height <= v:
            upper = v
            lower = grad_keys[i-1]
            pos = normalize(norm_height, lower, upper)
            break
    return pos, lower, upper


def calc_1d_gradient(norm_pos, normalized_gradients):
    grad_keys = sorted(list(normalized_gradients.keys()))
    inner_grad_ratio, lower_grad_key, upper_grad_key = select_inner_grad(norm_pos, grad_keys)
    lower_grad, upper_grad = normalized_gradients[lower_grad_key], normalized_gradients[upper_grad_key]
    # col = rgb_blend(inner_grad_ratio, lower_grad, upper_grad)
    # col = hls_blend(inner_grad_ratio, lower_grad, upper_grad)
    col = color_blend(inner_grad_ratio, lower_grad, upper_grad)
    return col

def calc_timeslice_gradients(theme_data):
    timeslice_gradients = {}
    for time, timeslice in theme_data.items():
        timeslice_gradients[time] = []
        g_min, g_max = min(timeslice.keys()), max(timeslice.keys())
        normalized_gradients = {normalize(x, g_min, g_max): y for x, y in timeslice.items()}
        # Normalized heights are the proportional heights for each row of LEDs
        normalized_heights = [(n/(h-1))for n in range(h)]
        for normalized_height in normalized_heights:
            timeslice_gradients[time].append(calc_1d_gradient(normalized_height, normalized_gradients))            
    return timeslice_gradients

def blend_between_times(ratio, lower, upper):
    blended = []
    for x in range(h):
        blended.append(color_blend(ratio, lower[x], upper[x]))
    return blended

theme_data = yaml.safe_load(open('themes/test.yaml', 'r'))
timeslice_gradients = calc_timeslice_gradients(theme_data)

# theme times are minutes +/- alarm_time
curr_time = 5.75  # just assume hours are floats, 5.75 = 5.45am
alarm_time = 6.50  # 6.30am
t_offset = (curr_time - alarm_time) * 60
# print(t_offset)  # time either side of alarm, in minutes
time_keys = sorted(timeslice_gradients.keys())
# Find ratio time between two times
time_ratio, lower_time_key, upper_time_key = select_inner_grad(t_offset, time_keys)


cols = blend_between_times(time_ratio, timeslice_gradients[lower_time_key], timeslice_gradients[upper_time_key])

gamma = [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
    1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
    2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
    5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
   10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
   17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
   25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
   37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
   51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
   69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
   90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
  115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
  144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
  177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
  215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255 ]

gamma_factor = 1.0
gamma = []
for i in range(255):
    gamma.append(int(pow(float(i) / float(255), gamma_factor) * 255 + 0.5))
    
# print(g)

o = '▓▓'
for t in range(min(time_keys), max(time_keys)):
    time_ratio, lower_time_key, upper_time_key = select_inner_grad(t, time_keys)
    cols = blend_between_times(time_ratio, timeslice_gradients[lower_time_key], timeslice_gradients[upper_time_key])
    for y in cols:
        # c = (int(y[0]),int(y[1]),int(y[2]),)
        c = (gamma[int(y[0])],gamma[int(y[1])],gamma[int(y[2])],)
        print(fg(c[0],c[1],c[2]) + bg(c[0],c[1],c[2]) + o + rs.fg + rs.bg, end="")
    print("")
