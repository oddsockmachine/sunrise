# Sunrise

_A smart sunrise lamp_

# Why

Because I find it hard to get out of bed when it's still dark out

# Modes/Features

- Sunrise: Takes a color scheme from a picture or timelapse, turns it into a timelapse to simulate the rising sun at a given time

- Clock: Overlays a faint digital clock on whatever else is happening - useful at night

- Moon: Shows the current phase of the moon

- Weather: Display a hint at what the weather will be like today

- Home Run: Displays the Giants logo if they hit a home run

- Doorbell: Visual alarm for when the doorbell rings

- KEXP: Mimics the lights in the KEXP studio

- Game of Life: Classic cellular automata

- Pong: CPU vs CPU game of pong

- Snake: demo game


# Usage

Run locally: `python sunrise local.json`

Run on the pi: `python sunrise pi.json`

Restart on pi: `ssh pi@sunrise "sudo systemctl restart sunrise"`


Send new configs to the node:

```sunrise_set ()
{
  foo=`cat $1` && mosquitto_pub -t sunrise -m $foo -h jarvis.local
}
```