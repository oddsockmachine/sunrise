from transforms import *
from json import load, loads
from sys import argv
import paho.mqtt.client as mqtt
from uptimer import UptimeProgram
from transforms.empty_grid import EmptyGrid
from transforms.sunrise import Sunrise
from transforms.sundial import Sundial
from transforms.clock_overlay import ClockOverlay
from transforms.brightness import Brightness
from transforms.gamma import Gamma
from transforms.orientation import Orientation
from transforms.circle_mask import CircleMask
from transforms.console_display import ConsoleDisplay
from transforms.diagonal import Diagonal
from transforms.neopixel_display import NeopixelDisplay
from transforms.refresh_rate import RefreshRate
from transforms.time import Time
from transforms.kexp import KEXP
from transforms.sky import Sky
from transforms.sun import Sun
from transforms.clouds import Clouds
from transforms.rain import Rain
from transforms.gitpull import GitPull
from transforms.murica import Murica
from transforms.giants import Giants
from transforms.fire import Fire
from transforms.pixellate import Pixellate

ctx = load(open(argv[1] if len(argv) > 1 else 'starting_config.json'))
CHANGE = False

def sub_cb(client, userdata, msg):
    print("?")
    global ctx
    global CHANGE
    CHANGE = True
    ctx = loads(msg.payload)

def context(_):
    global ctx
    return ctx

transform_lookup = {
    'ctx': context,
    'empty_grid': EmptyGrid,
    'sunrise': Sunrise,
    'sundial': Sundial,
    'clock_overlay': ClockOverlay,
    'brightness': Brightness,
    'gamma': Gamma,
    'orientation': Orientation,
    'circle_mask': CircleMask,
    'console_display': ConsoleDisplay,
    'diagonal': Diagonal,
    'neopixel_display': NeopixelDisplay,
    'refresh_rate': RefreshRate,
    'time': Time,
    'kexp': KEXP,
    'sky': Sky,
    'sun': Sun,
    'clouds': Clouds,
    'rain': Rain,
    'gitpull': GitPull,
    'murica': Murica,
    'giants': Giants,
    'fire': Fire,
    'pixellate': Pixellate,
}

# print(locals())
# exit()

def pipe(pipeline):
    ctx, grid = pipeline[0], None
    for stage in pipeline[1:]:
        ctx, grid = stage(ctx, grid)
    return ctx, grid

def gen_pipeline(ctx):
    return [transform_lookup[step](ctx) for step in ctx]

if __name__ == '__main__':
    client = mqtt.Client("sunrise")
    client.connect(ctx['ctx']['mqtt'])
    client.message_callback_add("sunrise", sub_cb)
    client.subscribe("sunrise")
    client.loop_start()
    uptimer = UptimeProgram(client).start()

    # TODO need a way to save old ctx temporarily, while a short-lived event happens
    # eg doorbell, message, homerun, etc
    run = True
    pipeline = gen_pipeline(ctx)
    while(run):
        if CHANGE:
            CHANGE = False
            pipeline = gen_pipeline(ctx)
            print("!")
        _, leds = pipe(pipeline)
        run = ctx['ctx']['run']
