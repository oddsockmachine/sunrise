from transforms.common import Chainable

class Giants(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.w = ctx['empty_grid']['size']
        self.h = ctx['empty_grid']['size']
        self.sprite = [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,],
                       [0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,],
                       [0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,],
                       [0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,],
                       [0,0,0,0,1,1,0,1,1,1,1,1,1,1,1,0,0,0,0,],
                       [0,0,0,0,1,1,0,1,1,1,1,1,1,1,1,0,0,0,0,],
                       [0,0,0,0,1,1,0,1,1,0,0,0,0,0,1,0,0,0,0,],
                       [0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,],
                       [0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,],
                       [0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,0,],
                       [0,0,0,0,1,1,0,1,1,1,1,0,1,1,0,0,0,0,0,],
                       [0,0,0,0,1,1,0,1,1,1,1,0,1,1,0,0,0,0,0,],
                       [0,0,0,0,1,1,0,1,1,0,0,0,1,1,0,0,0,0,0,],
                       [0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,],
                       [0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,],
                       [0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,],
                       [0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,],
                       [0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,],
                       [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,],]
        self.color = {1: ctx['giants'].get('color1', (254,90,29)), 0: ctx['giants'].get('color2', (30,30,30))}
        self.grid = [[(0,0,20)] * self.h for _ in range(self.w)]
        for y, row in enumerate(self.sprite):
                for x, led in enumerate(row):
                    self.grid[y][x] = self.color[led]

    def __call__(self, ctx: dict, grid: list):
        return ctx, self.grid
