import imp
from transforms.common import Chainable

class Clouds(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.speed = ctx['clouds'].get('speed', 0,5)
        self.coverage = ctx['clouds'].get('coverage', 0.5)
        self.brightness = ctx['clouds'].get('brightness', 0.5)
        self.w = ctx['empty_grid']['size']
        self.h = ctx['empty_grid']['size']
        self.grid = [[None] * self.h for _ in range(self.w)]
        self.cloud_sprites = [[
            [0,0,0,0,0,0,1,1,1,0,0,0,0,0],
            [0,0,0,1,1,1,1,1,1,0,1,0,0,0],
            [0,1,1,1,1,1,1,1,1,1,1,1,1,0],
            [1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        ],
        [
            [0,0,0,1,1,0,0,0,0],
            [0,0,1,1,1,1,1,1,0],
            [0,1,1,1,1,1,1,1,0],
            [1,1,1,1,1,1,1,1,1],
        ]]

    def __call__(self, ctx: dict, grid: list):
        return ctx, grid
