from transforms.common import Chainable
from sty import bg, fg, rs


class ConsoleDisplay(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.frame_skip = ctx['console_display'].get('frame_skip', 1)
        self.frame = 0
    def __call__(self, ctx: dict, grid: list):
        o = '▓▓▓'
        l = ""
        self.frame = (self.frame + 1) % self.frame_skip
        if self.frame != 0:
            return ctx, grid
        for row in grid:
            for x in row:
                # l += str(fg(x[0],x[1],x[2],) + o + rs.fg)
                l += str(fg(x[0],x[1],x[2],) + bg(x[0],x[1],x[2],) + o + rs.fg + rs.bg,)
            l += ('\n')
        print(l)
        return ctx, grid