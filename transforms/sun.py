from transforms.common import Chainable
from circle import create_mask

class Sun(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.w = ctx['empty_grid']['size']
        self.h = ctx['empty_grid']['size']
        self.color = (200,170,20)
        # self.grid = [[None] * self.h for _ in range(self.w)]
        self.sun_sprite = create_mask(7, 3.3)

    def __call__(self, ctx: dict, grid: list):
        # TODO Get position in sky
        # TODO control brightness
        x = 5
        y = 5
        for i, row in enumerate(self.sun_sprite):
            for j, pixel in enumerate(row):
                if pixel == True:
                    grid[i+x][y+j] = self.color
        return ctx, grid
