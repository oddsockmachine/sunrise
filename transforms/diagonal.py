from typing import DefaultDict
from transforms.common import Chainable
from circle import create_mask
from collections import defaultdict

class Diagonal(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.mask_radius = ctx['circle_mask']['radius']
        self.grid_size = ctx['empty_grid']['size']
        self.mask = create_mask(self.grid_size, self.mask_radius)
        self.strips = defaultdict(list)
        grid = [[(0,0,0)] * 12 for _ in range(self.grid_size)]
        for y, row in enumerate(grid):
            for x, led in enumerate(row):
                self.strips[x+y].append((x,y))
        self.chain_order = []
        for strip_num in sorted(self.strips.keys()):
            self.chain_order.extend(self.strips[strip_num] if strip_num % 2 else self.strips[strip_num][::-1])
        
    def __call__(self, ctx: dict, grid: list):
        if 'console_display' in self.ctx:
            chain = []
            for pos in self.chain_order:
                chain.append(grid[pos[1]][pos[0]])
            return ctx, grid
        # Transform grid into array
        if 'neopixel_display' in self.ctx:
            # zig-zagged strips need to be unwound
            # x + y coord = which diagonal strip
            chain = []
            for pos in self.chain_order:
                chain.append(grid[pos[1]][pos[0]])
            # for y, row in enumerate(grid):
            #     temp_row = []
            #     for x, led in enumerate(row):
            #         if self.mask[y][x]:
            #             temp_row.append(led)
            #     chain.extend(temp_row if (y % 2 == 1) else temp_row[::-1])
            print(chain)
            return ctx, chain
        return ctx, grid