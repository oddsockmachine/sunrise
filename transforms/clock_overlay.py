from transforms.common import Chainable
from digits import create_string

class ClockOverlay(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        # self.brightness = int(ctx['clock_overlay'].get('brightness'))
        self.color = ctx['clock_overlay']['color']
    def get_time(self):
        t = self.ctx['time']['curr_time']
        return t
    def __call__(self, ctx: dict, grid: list):
        curr_time = str(self.get_time())[:-3]
        s = create_string(curr_time)
        # brightness? border?
        position = (7, 0)
        # lookup = {
        #     0: (0,0,0), 1: (100,0,0)
        # }
        for y, row in enumerate(s):
            for x, bit in enumerate(row):
                if bit:
                    # grid[y+position[0]][x+position[1]] = lookup[bit]
                    a = grid[y+position[0]][x+position[1]]
                    # brightness = 15
                    b = (int(a[0]+self.color[0]),int(a[1]+self.color[1]),int(a[2]+self.color[2]))
                    grid[y+position[0]][x+position[1]] = b
        return ctx, grid
