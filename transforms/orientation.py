from transforms.common import Chainable

class Orientation(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
    def __call__(self, ctx: dict, grid: list):
        return ctx, grid
