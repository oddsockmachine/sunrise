from transforms.common import Chainable

class Sky(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.color = ctx['sky'].get('color', (50,50,200))
        self.w = ctx['empty_grid']['size']
        self.h = ctx['empty_grid']['size']
        self.grid = [[self.color] * self.h for _ in range(self.w)]

    def __call__(self, ctx: dict, grid: list):
        return ctx, self.grid
