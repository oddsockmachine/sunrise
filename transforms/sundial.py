from transforms.common import Chainable
from datetime import datetime
from requests import get
# 277 leds
# 240 = 24 hrs x 6 min blocks
# 33 leds spare
# ignore top and bottom 2 lines = 14 * 2 = 28
# 5 spare: middle 5 to signify noon?

# Get calendar events to display?

class Sundial(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.H = ctx['empty_grid']['height']
        self.W = ctx['empty_grid']['width']
        self.total_leds = self.H * self.W
        # print(self.total_leds)
        self.led_scaling_factor = self.total_leds / 24
        # print(self.led_scaling_factor)
        lat = ctx['sundial'].get("lat", "37.784713")
        lon = ctx['sundial'].get("lon", "-122.460310")
        day_data = get(f"https://api.sunrise-sunset.org/json?lat={lat}&lng={lon}&formatted=0").json()
        self.sunrise_time = day_data['results']['sunrise']
        self.sunset_time = day_data['results']['sunset']
        self.sunrise_led = self.get_led_for_time(self.sunrise_time)
        self.sunset_led = self.get_led_for_time(self.sunset_time)
        self.current_time_led = self.get_led_for_time(str(datetime.now()).replace(" ", "T"), False)

    def get_led_for_time(self, timestamp, shift=True):
        # TODO deal with offset, scaling, depending on grid size
        # print(timestamp)
        H, M, S = timestamp.split("T")[1].split("+")[0].split(":")
        # t = time(hour=int(H), minute=int(M))
        if shift:
            H = (int(H) - 7) % 24
        # print(H,M)
        num_led = int(((int(H) * 60) + int(M))/(24*60)*self.total_leds)
        # print(num_led)
        # num_leds = (int(H) * 6) + int(int(M)/10)
        # print(num_leds)
        # print(H, M)
        return num_led

    def __call__(self, ctx: dict, grid: list):
        self.current_time_led = self.get_led_for_time(str(datetime.now()).replace(" ", "T"), False)
        for y, row in enumerate(grid):
            for x, led in enumerate(row):
                num = x + (y * self.W)
                col = (10,10,30)
                if num == self.current_time_led:
                    if self.current_time_led > self.sunrise_led and self.current_time_led < self.sunset_led:
                        col = (200,200,40)
                    else:
                        col = (100,50,20)
                elif num > self.sunrise_led and num < self.sunset_led:
                    col = (50,50,200)
                grid[y][x] = col
        return ctx, grid


