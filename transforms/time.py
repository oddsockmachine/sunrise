from transforms.common import Chainable
from datetime import datetime, time


class Time(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
    def __call__(self, ctx: dict, grid: list):
        t = str(datetime.now().time()).split('.')[0].split(':')
        curr_time = time(int(t[0]), int(t[1]), int(t[2]))
        ctx['time']['curr_time'] = curr_time
        return ctx, grid
