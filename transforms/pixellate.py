from transforms.common import Chainable
from random import randint, shuffle
from circle import create_mask
from gradients import constrain_led

class Pixellate(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.w = ctx['empty_grid']['size']
        self.h = ctx['empty_grid']['size']
        self.speed = ctx['pixellate'].get('speed', 1)
        self.variability = ctx['pixellate'].get('variability', 10)
        self.difference = ctx['pixellate'].get('difference', 20)
        self.chance = ctx['pixellate'].get('chance', 100)
        self.color1 = (0,0,0)
        self.color2 = (100,100,100)
        self.size = self.w * self.h
        self.grid = [[self.color1] * self.h for _ in range(self.w)]
        self.mask_radius = ctx['circle_mask']['radius']
        self.grid_size = ctx['empty_grid']['size']
        self.mask = create_mask(self.grid_size, self.mask_radius)
        self.create_new_order()

    def __call__(self, ctx: dict, grid: list):
        if len(self.order) > 0:
            next = self.order.pop()
            x = int(next / self.w)
            y = int(next % self.h)                    
            self.grid[y][x] = (constrain_led(self.color2[0] + randint(-self.variability,self.variability)),
                                constrain_led(self.color2[1] + randint(-self.variability,self.variability)),
                                constrain_led(self.color2[2] + randint(-self.variability,self.variability)))
        else:
            self.create_new_order()
            self.color1 = self.color2
            r = randint(0,255)
            g = randint(0,r)
            b = 255 - (r + g)
            c = [r,g,b]
            shuffle(c)
            self.color2 = (c[0],c[1],c[2])
            d = self.difference
            self.color2 = (self.color1[0]+randint(-d,d),self.color1[1]+randint(-d,d),self.color1[2]+randint(-d,d))
        return ctx, self.grid

    def create_new_order(self):
        self.order = []
        order = list(range(self.size))
        for o in order:
            x = int(o / self.w)
            y = int(o % self.h)
            self.order.append(o)
        shuffle(self.order)
