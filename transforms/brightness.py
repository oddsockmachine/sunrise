from transforms.common import Chainable
from gradients import constrain_led


class Brightness(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.brightness = ctx['brightness']['brightness']

    def __call__(self, ctx: dict, grid: list):
        for y, row in enumerate(grid):
            for x, led in enumerate(row):
                # grid[y][x] = (self.g[constrain_led(led[0])],self.g[constrain_led(led[1])],self.g[constrain_led(led[2])],)
                grid[y][x] = (constrain_led(led[0]*self.brightness),
                            constrain_led(led[1]*self.brightness),
                            constrain_led(led[2]*self.brightness),
                            )
        return ctx, grid
