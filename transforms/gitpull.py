from transforms.common import Chainable
from os import popen
from time import sleep

class GitPull(Chainable):
    # Call to initiate a git pull. Only useful for pulling updated themes, not code
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.done = False
    def __call__(self, ctx: dict, grid: list):
        if not self.done:
            popen('git pull origin master')
            # popen('git -C /home/pi/sunrise pull origin master')
            sleep(3)
            self.done = True
        return ctx, grid


