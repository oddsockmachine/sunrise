from transforms.common import Chainable
from circle import create_mask

class CircleMask(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.mask_radius = ctx['circle_mask']['radius']
        self.grid_size = ctx['empty_grid']['size']
        self.mask = create_mask(self.grid_size, self.mask_radius)
        # print(self.mask)
    def __call__(self, ctx: dict, grid: list):
        if 'console_display' in self.ctx:
            for y, row in enumerate(grid):
                for x, led in enumerate(row):
                    if not self.mask[y][x]:
                        grid[y][x] = (0,0,0)
            return ctx, grid
        # Transform grid into array, by masking
        if 'neopixel_display' in self.ctx:
            # zig-zagged strips need to be unwound
            chain = []
            for y, row in enumerate(grid):
                temp_row = []
                for x, led in enumerate(row):
                    if self.mask[y][x]:
                        temp_row.append(led)
                chain.extend(temp_row if (y % 2 == 1) else temp_row[::-1])
            return ctx, chain
        return ctx, grid