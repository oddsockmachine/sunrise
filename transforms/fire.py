from transforms.common import Chainable
from transforms.utils import interpolate, fade
from random import choice, randint

class Fire(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.w = ctx['empty_grid']['size']
        self.h = ctx['empty_grid']['size']
        self.grid = [[(0,0,0)] * self.h for _ in range(self.w)]
        self.flame_colors = [(238,153,17, 50), (240,185,4, 50), (107,35,4, 50), (210,111,4, 50)]
        self.flames = [[None] * self.h for _ in range(self.w)]
        for y in range(self.w):
            for x in range(self.h):
                self.flames[y][x] = fade((0,0,0,0),choice(self.flame_colors),100,100)

    def __call__(self, ctx: dict, grid: list):
        for y, row in enumerate(self.flames):
            for x, f in enumerate(row):
                if f.remaining == 0:
                    f.last = f.next
                    f.next = choice(self.flame_colors)
                    f.steps = randint(5,30)  # todo ctx>fire>steps>max/min
                    f.remaining = f.steps
                else:
                    f.remaining -= 1
                r = 1-(f.remaining / f.steps)
                c = interpolate(f.last, f.next, r)
                self.grid[y][x] = c

        return ctx, self.grid