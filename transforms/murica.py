from transforms.common import Chainable


class Murica(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.w = ctx['empty_grid']['size']
        self.h = ctx['empty_grid']['size']
        w = (100,100,100)
        r = (150,50,50)
        b = (50,50,150)
        # w
        self.grid = [[w] * self.h for _ in range(self.w)]
        # r
        for y, row in enumerate(self.grid):
            if y % 2 == 0:
                for x, led in enumerate(row):
                    self.grid[y][x] = r
        # b
        for y, row in enumerate(self.grid[:9]):
            for x, led in enumerate(row[:9]):
                    self.grid[y][x] = b
        # w
        for y, row in enumerate(self.grid[:9]):
            if y % 2 == 1:
                for x, led in enumerate(row[:9]):
                    if x % 2 == 1:
                        self.grid[y][x] = w

    def __call__(self, ctx: dict, grid: list):
        # stars & stripes
        return ctx, self.grid