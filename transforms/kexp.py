from transforms.common import Chainable
from random import randint

class KEXP(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.speed = ctx['kexp'].get('speed', 1)
        self.chance = ctx['kexp'].get('chance', 100)
        self.color = ctx['kexp'].get('color', (50,50,50))
        self.w = ctx['empty_grid']['size']
        self.h = ctx['empty_grid']['size']
        self.grid = [[None] * self.h for _ in range(self.w)]
    def __call__(self, ctx: dict, grid: list):
        for y, row in enumerate(grid):
            for x, led in enumerate(row):
                if randint(0,self.chance) == 0:
                    self.grid[y][x] = -1
                i = self.grid[y][x]
                if i == None:
                    c = grid[y][x]
                else:
                    intensity = (i * (100 if i < 0 else -100)) + 100
                    c = (self.color[0]*(intensity/100), self.color[1]*(intensity/100), self.color[2]*(intensity/100))
                    self.grid[y][x] += self.speed / 100
                    if self.grid[y][x] > 1:
                        self.grid[y][x] = None
                grid[y][x] = (c[0],c[1],c[2])
        return ctx, grid
