from transforms.common import Chainable
from datetime import time
import yaml
from gradients import get_gradient_for_timeslice, calc_timeslice_gradients


class Sunrise(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        theme = ctx['sunrise']['theme']
        alarm_time = ctx['sunrise']['alarm_time']
        self.theme_data = yaml.safe_load(open(f'themes/{theme}.yaml', 'r'))
        self.alarm_time = time(int(alarm_time.split(':')[0]), int(alarm_time.split(':')[1]))
        self.height = ctx['empty_grid']['height']
        self.timeslice_gradients = calc_timeslice_gradients(self.theme_data, self.height)
    def get_time(self):
        t = self.ctx['time']['curr_time']
        return t
    def time_to_alarm(self) -> float:
        # Return a fractional number of minutes until/after the alarm time
        a = self.alarm_time
        t = self.get_time()
        diff_mins = ((t.hour - a.hour) * 60) + (t.minute - a.minute) + ((t.second - a.second)/60)
        if diff_mins > 180:  # ie 3 hrs past alarm clock time
            diff_mins -= 24 * 60  # roll back 1 day
        return diff_mins
    def __call__(self, ctx: dict, grid: list):
        time_remaining = self.time_to_alarm()
        cols = get_gradient_for_timeslice(time_remaining, self.height, self.timeslice_gradients)
        for y, row in enumerate(grid):
            for x, led in enumerate(row):
                grid[y][x] = cols[y]
        return ctx, grid