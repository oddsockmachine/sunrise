from transforms.common import Chainable

class EmptyGrid(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.w = ctx['empty_grid']['size']
        self.h = ctx['empty_grid']['size']
        self.shade = ctx['empty_grid'].get('shade', 0)
    def __call__(self, ctx: dict, grid: list):
        grid = [[(self.shade,self.shade,self.shade)] * self.h for _ in range(self.w)]
        # grid = [[(0,0,0)] * 12 for _ in range(12)]
        return ctx, grid
