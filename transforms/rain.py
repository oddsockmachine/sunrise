from transforms.common import Chainable

class Rain(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.speed = ctx['rain']['speed']
        self.w = ctx['empty_grid']['size']
        self.h = ctx['empty_grid']['size']
        self.grid = [[None] * self.h for _ in range(self.w)]

    def __call__(self, ctx: dict, grid: list):
        # rain drops falling down
        return ctx, grid
