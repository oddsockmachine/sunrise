from transforms.common import Chainable
import imp
from time import sleep

class RefreshRate(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
    def __call__(self, ctx: dict, grid: list):
        fps = self.ctx['refresh_rate']['fps']
        sleep(1/fps)
        return ctx, grid
