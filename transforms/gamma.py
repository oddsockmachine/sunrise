from transforms.common import Chainable
from gradients import constrain_led

class Gamma(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.gamma_factor = ctx['gamma']['gamma_factor']
        self.gamma_transform = []
        for i in range(256):
            self.gamma_transform.append(int(pow(float(i) / float(255), self.gamma_factor) * 255 + 0.5))
        self.g = self.gamma_transform
    def __call__(self, ctx: dict, grid: list):
        for y, row in enumerate(grid):
            for x, led in enumerate(row):
                grid[y][x] = (self.g[constrain_led(led[0])],self.g[constrain_led(led[1])],self.g[constrain_led(led[2])],)
        return ctx, grid
