from recordclass import recordclass
fade = recordclass('fade', 'last next steps remaining')

def interpolate(a, b, r):
    # print(a,b,r)
    return (abs(int((a[0] - b[0]) * r) - a[0]),
            abs(int((a[1] - b[1]) * r) - a[1]),
            abs(int((a[2] - b[2]) * r) - a[2]),
            abs(int((a[3] - b[3]) * r) - a[3]))

