from transforms.clouds import Clouds
from transforms.common import Chainable
from datetime import datetime, time
from sty import bg, fg, rs
from circle import create_mask
from time import sleep
import yaml
# from gradients import get_gradient_for_timeslice, calc_timeslice_gradients, constrain_led
from digits import create_string
from random import randint, choice
# from os import popen

from transforms import *



# from recordclass import recordclass
# fade = recordclass('fade', 'last next steps remaining')

# def interpolate(a, b, r):
#     # print(a,b,r)
#     return (abs(int((a[0] - b[0]) * r) - a[0]),
#             abs(int((a[1] - b[1]) * r) - a[1]),
#             abs(int((a[2] - b[2]) * r) - a[2]),
#             abs(int((a[3] - b[3]) * r) - a[3]))



# class Chainable():
#     def __init__(self, ctx) -> None:
#         self.ctx = ctx
#         pass
#     # def __call__(self, *args: Any, **kwds: Any) -> Any:
#     def __call__(self, ctx: dict, grid: list):
#         return ctx, grid

# class Time(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#     def __call__(self, ctx: dict, grid: list):
#         t = str(datetime.now().time()).split('.')[0].split(':')
#         curr_time = time(int(t[0]), int(t[1]), int(t[2]))
#         ctx['time']['curr_time'] = curr_time
#         return ctx, grid

class ReminderOverlay(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
    def __call__(self, ctx: dict, grid: list):
        return ctx, grid

# class ClockOverlay(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         # self.brightness = int(ctx['clock_overlay'].get('brightness'))
#         self.color = ctx['clock_overlay']['color']
#     def get_time(self):
#         t = self.ctx['time']['curr_time']
#         return t
#     def __call__(self, ctx: dict, grid: list):
#         curr_time = str(self.get_time())[:-3]
#         s = create_string(curr_time)
#         # brightness? border?
#         position = (7, 0)
#         # lookup = {
#         #     0: (0,0,0), 1: (100,0,0)
#         # }
#         for y, row in enumerate(s):
#             for x, bit in enumerate(row):
#                 if bit:
#                     # grid[y+position[0]][x+position[1]] = lookup[bit]
#                     a = grid[y+position[0]][x+position[1]]
#                     # brightness = 15
#                     b = (int(a[0]+self.color[0]),int(a[1]+self.color[1]),int(a[2]+self.color[2]))
#                     grid[y+position[0]][x+position[1]] = b
#         return ctx, grid

# class Sunrise(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         theme = ctx['sunrise']['theme']
#         alarm_time = ctx['sunrise']['alarm_time']
#         self.theme_data = yaml.safe_load(open(f'themes/{theme}.yaml', 'r'))
#         self.alarm_time = time(int(alarm_time.split(':')[0]), int(alarm_time.split(':')[1]))
#         self.height = ctx['empty_grid']['height']
#         self.timeslice_gradients = calc_timeslice_gradients(self.theme_data, self.height)
#     def get_time(self) -> Time:
#         t = self.ctx['time']['curr_time']
#         return t
#     def time_to_alarm(self) -> float:
#         # Return a fractional number of minutes until/after the alarm time
#         a = self.alarm_time
#         t = self.get_time()
#         diff_mins = ((t.hour - a.hour) * 60) + (t.minute - a.minute) + ((t.second - a.second)/60)
#         if diff_mins > 180:  # ie 3 hrs past alarm clock time
#             diff_mins -= 24 * 60  # roll back 1 day
#         return diff_mins
#     def __call__(self, ctx: dict, grid: list):
#         time_remaining = self.time_to_alarm()
#         cols = get_gradient_for_timeslice(time_remaining, self.height, self.timeslice_gradients)
#         for y, row in enumerate(grid):
#             for x, led in enumerate(row):
#                 grid[y][x] = cols[y]
#         return ctx, grid

# class RefreshRate(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#     def __call__(self, ctx: dict, grid: list):
#         fps = self.ctx['refresh_rate']['fps']
#         sleep(1/fps)
#         return ctx, grid

# class ConsoleDisplay(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.frame_skip = ctx['console_display'].get('frame_skip', 1)
#         self.frame = 0
#     def __call__(self, ctx: dict, grid: list):
#         o = '▓▓▓'
#         l = ""
#         self.frame = (self.frame + 1) % self.frame_skip
#         if self.frame != 0:
#             return ctx, grid
#         for row in grid:
#             for x in row:
#                 # l += str(fg(x[0],x[1],x[2],) + o + rs.fg)
#                 l += str(fg(x[0],x[1],x[2],) + bg(x[0],x[1],x[2],) + o + rs.fg + rs.bg,)
#             l += ('\n')
#         print(l)
#         return ctx, grid

# class NeopixelDisplay(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         import board
#         import neopixel
#         self.num_pixels = ctx['neopixel_display']['num']
#         # lookup pin from ctx
#         pins = {"D0":board.D0,"D1":board.D1,"D2":board.D2,"D3":board.D3,"D4":board.D4,"D5":board.D5,"D6":board.D6,"D7":board.D7,"D8":board.D8,"D9":board.D9,"D10":board.D10,"D11":board.D11,"D12":board.D12,"D13":board.D13,"D14":board.D14,"D15":board.D15,"D16":board.D16,"D17":board.D17,"D18":board.D18,"D19":board.D19,"D20":board.D20,"D21":board.D21,"D22":board.D22,"D23":board.D23,"D24":board.D24,"D25":board.D25,"D26":board.D26,"D27":board.D27,}
#         self.pin = pins[ctx['neopixel_display']['pin']]
#         self.pixels = neopixel.NeoPixel(self.pin, self.num_pixels, auto_write=False)
#     def __call__(self, ctx: dict, grid: list):
#         for i in range(self.num_pixels):
#             self.pixels[i] = grid[i]
#         self.pixels.show()
#         return ctx, grid

# class CircleMask(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.mask_radius = ctx['circle_mask']['radius']
#         self.grid_size = ctx['empty_grid']['size']
#         self.mask = create_mask(self.grid_size, self.mask_radius)
#     def __call__(self, ctx: dict, grid: list):
#         if 'console_display' in self.ctx:
#             for y, row in enumerate(grid):
#                 for x, led in enumerate(row):
#                     if not self.mask[y][x]:
#                         grid[y][x] = (0,0,0)
#             return ctx, grid
#         # Transform grid into array, by masking
#         if 'neopixel_display' in self.ctx:
#             # zig-zagged strips need to be unwound
#             chain = []
#             for y, row in enumerate(grid):
#                 temp_row = []
#                 for x, led in enumerate(row):
#                     if self.mask[y][x]:
#                         temp_row.append(led)
#                 chain.extend(temp_row if (y % 2 == 1) else temp_row[::-1])
#             return ctx, chain
#         return ctx, grid

# class Orientation(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#     def __call__(self, ctx: dict, grid: list):
#         return ctx, grid

# class Gamma(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.gamma_factor = ctx['gamma']['gamma_factor']
#         self.gamma_transform = []
#         for i in range(256):
#             self.gamma_transform.append(int(pow(float(i) / float(255), self.gamma_factor) * 255 + 0.5))
#         self.g = self.gamma_transform
#     def __call__(self, ctx: dict, grid: list):
#         for y, row in enumerate(grid):
#             for x, led in enumerate(row):
#                 grid[y][x] = (self.g[constrain_led(led[0])],self.g[constrain_led(led[1])],self.g[constrain_led(led[2])],)
#         return ctx, grid

# class Brightness(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#     def __call__(self, ctx: dict, grid: list):
#         return ctx, grid

# class EmptyGrid(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.w = ctx['empty_grid']['size']
#         self.h = ctx['empty_grid']['size']
#         self.shade = ctx['empty_grid'].get('shade', 0)
#     def __call__(self, ctx: dict, grid: list):
#         grid = [[(self.shade,self.shade,self.shade)] * self.h for _ in range(self.w)]
#         return ctx, grid

# class KEXP(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.speed = ctx['kexp'].get('speed', 1)
#         self.chance = ctx['kexp'].get('chance', 100)
#         self.color = ctx['kexp'].get('color', (50,50,50))
#         self.w = ctx['empty_grid']['size']
#         self.h = ctx['empty_grid']['size']
#         self.grid = [[None] * self.h for _ in range(self.w)]
#     def __call__(self, ctx: dict, grid: list):
#         for y, row in enumerate(grid):
#             for x, led in enumerate(row):
#                 if randint(0,self.chance) == 0:
#                     self.grid[y][x] = -1
#                 i = self.grid[y][x]
#                 if i == None:
#                     c = grid[y][x]
#                 else:
#                     intensity = (i * (100 if i < 0 else -100)) + 100
#                     c = (self.color[0]*(intensity/100), self.color[1]*(intensity/100), self.color[2]*(intensity/100))
#                     self.grid[y][x] += self.speed / 100
#                     if self.grid[y][x] > 1:
#                         self.grid[y][x] = None
#                 grid[y][x] = (c[0],c[1],c[2])
#         return ctx, grid


# class Squid(Chainable):
#     # squidsoup style rainbow
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.speed = ctx['squid'].get('speed', 10)
#         self.variability = ctx['squid'].get('variability', 10)
#         self.color = ctx['kexp'].get('color', (50,50,50))
#         self.w = ctx['empty_grid']['size']
#         self.h = ctx['empty_grid']['size']
#         self.grid = [[None] * self.h for _ in range(self.w)]
#     def __call__(self, ctx: dict, grid: list):
#         for y, row in enumerate(grid):
#             for x, led in enumerate(row):
#                 if randint(0,self.chance) == 0:
#                     self.grid[y][x] = -1
#                 i = self.grid[y][x]
#                 if i == None:
#                     c = grid[y][x]
#                 else:
#                     intensity = (i * (100 if i < 0 else -100)) + 100
#                     c = (self.color[0]*(intensity/100), self.color[1]*(intensity/100), self.color[2]*(intensity/100))
#                     self.grid[y][x] += self.speed / 100
#                     if self.grid[y][x] > 1:
#                         self.grid[y][x] = None
#                 grid[y][x] = (c[0],c[1],c[2])
#         return ctx, grid


class GOL(Chainable):
    def __init__(self, ctx) -> None:
        super().__init__(ctx)
        self.speed = ctx['gol']['speed']
        self.color = ctx['gol']['color']
        self.w = ctx['empty_grid']['size']
        self.h = ctx['empty_grid']['size']
        self.grid = [[None] * self.h for _ in range(self.w)]

    def __call__(self, ctx: dict, grid: list):
        return ctx, grid


# class Sky(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.color = ctx['sky'].get('color', (50,50,200))
#         self.w = ctx['empty_grid']['size']
#         self.h = ctx['empty_grid']['size']
#         self.grid = [[self.color] * self.h for _ in range(self.w)]
    # def __call__(self, ctx: dict, grid: list):
    #     return ctx, self.grid


# class Clouds(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.speed = ctx['clouds'].get('speed', 0,5)
#         self.coverage = ctx['clouds'].get('coverage', 0.5)
#         self.brightness = ctx['clouds'].get('brightness', 0.5)
#         self.w = ctx['empty_grid']['size']
#         self.h = ctx['empty_grid']['size']
#         self.grid = [[None] * self.h for _ in range(self.w)]
#         self.cloud_sprites = [[
#             [0,0,0,0,0,0,1,1,1,0,0,0,0,0],
#             [0,0,0,1,1,1,1,1,1,0,1,0,0,0],
#             [0,1,1,1,1,1,1,1,1,1,1,1,1,0],
#             [1,1,1,1,1,1,1,1,1,1,1,1,1,1],
#         ],
#         [
#             [0,0,0,1,1,0,0,0,0],
#             [0,0,1,1,1,1,1,1,0],
#             [0,1,1,1,1,1,1,1,0],
#             [1,1,1,1,1,1,1,1,1],
#         ]]

#     def __call__(self, ctx: dict, grid: list):
#         return ctx, grid


# class Sun(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.w = ctx['empty_grid']['size']
#         self.h = ctx['empty_grid']['size']
#         self.color = (200,170,20)
#         # self.grid = [[None] * self.h for _ in range(self.w)]
#         self.sun_sprite = create_mask(7, 3.3)

#     def __call__(self, ctx: dict, grid: list):
#         # TODO Get position in sky
#         # TODO control brightness
#         x = 5
#         y = 5
#         for i, row in enumerate(self.sun_sprite):
#             for j, pixel in enumerate(row):
#                 if pixel == True:
#                     grid[i+x][y+j] = self.color
#         return ctx, grid

# class Rain(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.speed = ctx['rain']['speed']
#         self.w = ctx['empty_grid']['size']
#         self.h = ctx['empty_grid']['size']
#         self.grid = [[None] * self.h for _ in range(self.w)]

#     def __call__(self, ctx: dict, grid: list):
#         # rain drops falling down
#         return ctx, grid

# class Murica(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.w = ctx['empty_grid']['size']
#         self.h = ctx['empty_grid']['size']
#         w = (100,100,100)
#         r = (150,50,50)
#         b = (50,50,150)
#         # w
#         self.grid = [[w] * self.h for _ in range(self.w)]
#         # r
#         for y, row in enumerate(self.grid):
#             if y % 2 == 0:
#                 for x, led in enumerate(row):
#                     self.grid[y][x] = r
#         # b
#         for y, row in enumerate(self.grid[:9]):
#             for x, led in enumerate(row[:9]):
#                     self.grid[y][x] = b
#         # w
#         for y, row in enumerate(self.grid[:9]):
#             if y % 2 == 1:
#                 for x, led in enumerate(row[:9]):
#                     if x % 2 == 1:
#                         self.grid[y][x] = w

#     def __call__(self, ctx: dict, grid: list):
#         # stars & stripes
#         return ctx, self.grid


# class Giants(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.w = ctx['empty_grid']['size']
#         self.h = ctx['empty_grid']['size']
#         self.sprite = [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,],
#                        [0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,],
#                        [0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,],
#                        [0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,],
#                        [0,0,0,0,1,1,0,1,1,1,1,1,1,1,1,0,0,0,0,],
#                        [0,0,0,0,1,1,0,1,1,1,1,1,1,1,1,0,0,0,0,],
#                        [0,0,0,0,1,1,0,1,1,0,0,0,0,0,1,0,0,0,0,],
#                        [0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,],
#                        [0,0,0,0,0,0,1,1,1,1,1,1,1,1,0,0,0,0,0,],
#                        [0,0,0,0,0,0,0,1,1,0,0,0,1,1,0,0,0,0,0,],
#                        [0,0,0,0,1,1,0,1,1,1,1,0,1,1,0,0,0,0,0,],
#                        [0,0,0,0,1,1,0,1,1,1,1,0,1,1,0,0,0,0,0,],
#                        [0,0,0,0,1,1,0,1,1,0,0,0,1,1,0,0,0,0,0,],
#                        [0,0,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,],
#                        [0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,],
#                        [0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,],
#                        [0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,],
#                        [0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,],
#                        [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,],]
#         self.color = {1: ctx['giants'].get('color1', (254,90,29)), 0: ctx['giants'].get('color2', (30,30,30))}
#         self.grid = [[(0,0,20)] * self.h for _ in range(self.w)]
#         for y, row in enumerate(self.sprite):
#                 for x, led in enumerate(row):
#                     self.grid[y][x] = self.color[led]

#     def __call__(self, ctx: dict, grid: list):
#         return ctx, self.grid

# class Fire(Chainable):
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.w = ctx['empty_grid']['size']
#         self.h = ctx['empty_grid']['size']
#         self.grid = [[(0,0,0)] * self.h for _ in range(self.w)]
#         self.flame_colors = [(238,153,17, 50),  	(240,185,4, 50),  	(107,35,4, 50), (0, 0, 0, 20), 	(210,111,4, 50)]
#         self.flames = [[None] * self.h for _ in range(self.w)]
#         for y in range(self.w):
#             for x in range(self.h):
#                 self.flames[y][x] = fade((0,0,0,0),choice(self.flame_colors),100,100)

#     def __call__(self, ctx: dict, grid: list):
#         for y, row in enumerate(self.flames):
#             for x, f in enumerate(row):
#                 if f.remaining == 0:
#                     f.last = f.next
#                     f.next = choice(self.flame_colors)
#                     f.steps = randint(5,30)  # todo ctx>fire>steps>max/min
#                     f.remaining = f.steps
#                 else:
#                     f.remaining -= 1
#                 r = 1-(f.remaining / f.steps)
#                 c = interpolate(f.last, f.next, r)
#                 self.grid[y][x] = c

#         return ctx, self.grid

# class GitPull(Chainable):
#     # Call to initiate a git pull. Only useful for pulling updated themes, not code
#     def __init__(self, ctx) -> None:
#         super().__init__(ctx)
#         self.done = False
#     def __call__(self, ctx: dict, grid: list):
#         if not self.done:
#             popen('git pull origin master')
#             # popen('git -C /home/pi/sunrise pull origin master')
#             sleep(3)
#             self.done = True
#         return ctx, grid


