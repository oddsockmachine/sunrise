from pprint import pprint
from math import sqrt, pow, floor

# GRID_SIZE = 19
LED = "▓▓"
# MIDPOINT = 9
# RADIUS = 7

def create_mask(grid_size, radius):
    count = 0
    midpoint = floor(grid_size/2)
    grid = [[False for x in range(grid_size)] for y in range(grid_size)]

    for y, row in enumerate(grid):
        for x, led in enumerate(row):
            distance = sqrt( pow(y-midpoint,2) + pow(x-midpoint,2) )
            if distance < radius:
                grid[y][x] = True
                count +=1
    return grid

if __name__ == '__main__':
    counter = 0
    for y in create_mask(19, 9.3):
        print("".join([LED if x else "  " for x in y]), end=" ")
        leds = len(list(filter(lambda x: x, y)))
        counter += leds
        print(leds, counter)
    # print(count)

MASK = create_mask(19, 9.3)