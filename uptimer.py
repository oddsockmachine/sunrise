from datetime import datetime
from time import sleep
from threading import Thread

class ActorThread(Thread):
    def __init__(self, name=None):
        if not name:
            name=self.__class__.__name__.lower()
        Thread.__init__(self, name=name)
        self.daemon = True
        self.keep_running = True
        self.name = name
    
    def start(self):
        super().start()
        return self

    def run(self):
        print(f"{self.__class__.__name__} started")
        while self.keep_running:
            self.event_loop()
        print(f"{self.__class__.__name__} killed")
        return



class UptimeProgram(ActorThread):
    def __init__(self, client, topic="uptime/sunrise"):
        super().__init__()
        self.topic = topic
        self.client = client
        self.start_time = datetime.now()
        return
    def event_loop(self):
        runtime = str(datetime.now() - self.start_time).split('.')[0]
        # print(runtime)
        self.client.publish(self.topic, runtime)
        sleep(30)
