
import yaml
from pprint import pprint
import colorsys
from sty import bg, fg, rs

w = 10
h = 30

def color_blend(x, l, u):
    a = False
    if a:
        return hls_blend(x,l,u)
    else:
        return rgb_blend(x,l,u)

def normalize(x, min, max):
    g_range = max - min
    offset = x - min
    n = offset / g_range
    return n

def linear_blend(x, l, u):
    if l == u:
        return l
    b = l + ((u - l) * x)
    # print(f"x: {x}, l: {l}, u: {u}  =  {b}")
    return b

def hue_blend(x, lower, upper):
    if upper - lower > 0.5:
        upper -= 1
        # print("!")
        return 1+linear_blend(x, lower, upper)
    return linear_blend(x, lower, upper)

def hls_blend(x, l, u):
    hls_l = colorsys.rgb_to_hls(l[0],l[1],l[2])
    hls_u = colorsys.rgb_to_hls(u[0],u[1],u[2])
    # print(hls_l)
    blend = (
        hue_blend(x, hls_l[0], hls_u[0]),
        linear_blend(x, hls_l[1], hls_u[1]),
        linear_blend(x, hls_l[2], hls_u[2]),
    )
    rgb = colorsys.hls_to_rgb(blend[0], blend[1], blend[2])
    return rgb

def rgb_blend(x, l, u):
    rgb = (
        linear_blend(x, l[0], u[0]),
        linear_blend(x, l[1], u[1]),
        linear_blend(x, l[2], u[2]),
    )
    return rgb

def select_inner_grad(norm_height, grad_keys):
    # Convert from a multi-part gradient to a single color pair w ratio position
    if norm_height in grad_keys:
        # Special case where position is exactly aligned with a key
        lower = norm_height
        upper = norm_height
        pos = 0.5
        return pos, lower, upper
    if norm_height > (max(grad_keys)):
        norm_height = max(grad_keys)
    if norm_height < (min(grad_keys)):
        norm_height = min(grad_keys)
    for i, v in enumerate(grad_keys):
        if norm_height <= v:
            upper = v
            lower = grad_keys[i-1]
            pos = normalize(norm_height, lower, upper)
            break
    return pos, lower, upper


def calc_1d_gradient(norm_pos, normalized_gradients):
    grad_keys = sorted(list(normalized_gradients.keys()))
    inner_grad_ratio, lower_grad_key, upper_grad_key = select_inner_grad(norm_pos, grad_keys)
    lower_grad, upper_grad = normalized_gradients[lower_grad_key], normalized_gradients[upper_grad_key]
    # col = rgb_blend(inner_grad_ratio, lower_grad, upper_grad)
    # col = hls_blend(inner_grad_ratio, lower_grad, upper_grad)
    col = color_blend(inner_grad_ratio, lower_grad, upper_grad)
    return col

def calc_timeslice_gradients(theme_data, height):
    timeslice_gradients = {}
    for time, timeslice in theme_data.items():
        timeslice_gradients[time] = []
        g_min, g_max = min(timeslice.keys()), max(timeslice.keys())
        normalized_gradients = {normalize(x, g_min, g_max): y for x, y in timeslice.items()}
        # Normalized heights are the proportional heights for each row of LEDs
        normalized_heights = [(n/(height-1))for n in range(height)]
        for normalized_height in normalized_heights:
            timeslice_gradients[time].append(calc_1d_gradient(normalized_height, normalized_gradients))            
    return timeslice_gradients

def blend_between_times(ratio, lower, upper, height):
    blended = []
    for x in range(height):
        blended.append(color_blend(ratio, lower[x], upper[x]))
    return blended

def get_gradient_for_timeslice(t_offset, height, timeslice_gradients):
    # As below, but timeslices have been precalculated and cached to speed up computation
    time_keys = sorted(timeslice_gradients.keys())
    time_ratio, lower_time_key, upper_time_key = select_inner_grad(t_offset, time_keys)
    cols = blend_between_times(time_ratio, timeslice_gradients[lower_time_key], timeslice_gradients[upper_time_key], height)
    return cols

def get_gradient_for_time(t_offset, theme_data, height):
    # theme times are minutes +/- alarm_time
    # t_offset = (curr_time - alarm_time) * 60
    # print(t_offset)  # time either side of alarm, in minutes
    # Find ratio time between two times
    # TODO pull out timeslice_grads, cache in self
    timeslice_gradients = calc_timeslice_gradients(theme_data, height)
    time_keys = sorted(timeslice_gradients.keys())
    time_ratio, lower_time_key, upper_time_key = select_inner_grad(t_offset, time_keys)
    cols = blend_between_times(time_ratio, timeslice_gradients[lower_time_key], timeslice_gradients[upper_time_key], height)
    return cols

def constrain_led(l):
    return min(255, max(0, int(l)))
    
# curr_time = 5.75  # just assume hours are floats, 5.75 = 5.45am
# alarm_time = 6.50  # 6.30am
# theme_data = yaml.safe_load(open('themes/test.yaml', 'r'))



# o = '▓▓'
# for t in range(min(time_keys), max(time_keys)):
#     time_ratio, lower_time_key, upper_time_key = select_inner_grad(t, time_keys)
#     cols = blend_between_times(time_ratio, timeslice_gradients[lower_time_key], timeslice_gradients[upper_time_key])
#     for y in cols:
#         c = (int(y[0]),int(y[1]),int(y[2]),)
#         print(fg(c[0],c[1],c[2]) + bg(c[0],c[1],c[2]) + o + rs.fg + rs.bg, end="")
#     print("")
