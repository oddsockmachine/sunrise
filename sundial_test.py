from datetime import datetime
import yaml
from requests import get
from pprint import pprint


def get_led_for_time(timestamp, shift=True):
    # print(timestamp)
    H, M, S = timestamp.split("T")[1].split("+")[0].split(":")
    # t = time(hour=int(H), minute=int(M))
    if shift:
        H = (int(H) - 7) % 24
    num_leds = (int(H) * 6) + int(int(M)/10)
    # print(H, M)
    return num_leds

ctx = {}

print(datetime.now())

lat = ctx.get("lat", "37.784713")
lon = ctx.get("lon", "-122.460310")
day_data = get(f"https://api.sunrise-sunset.org/json?lat={lat}&lng={lon}&formatted=0").json()
pprint(day_data)
sunrise_time = day_data['results']['sunrise']
sunset_time = day_data['results']['sunset']
# print(sunrise_time)
# print(sunset_time)
sunrise_led = get_led_for_time(sunrise_time)
sunset_led = get_led_for_time(sunset_time)
current_time_led = get_led_for_time(str(datetime.now()).replace(" ", "T"), False)
print(f"sunrise: {sunrise_led}")
print(f"sunset:  {sunset_led}")
print(f"current: {current_time_led}")
offset = 28
total = 240
# pre_dawn_leds = range(sunrise_led)
# post_dusk_leds = range(sunset_led, total)

